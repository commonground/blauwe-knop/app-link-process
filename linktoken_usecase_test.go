// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package applinkprocess_test

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"applinkprocess"
	"applinkprocess/mock"
)

const DummyOIN = "0000000000"

var DummyLinkToken = applinkprocess.LinkToken("dummy-link-token")

var DummyOrganization = applinkprocess.Organization{
	OIN: DummyOIN,
}

func generateLinkTokenRepository(t *testing.T) *mock.MockLinkTokenRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockLinkTokenRepository(ctrl)
	return repo
}

func generateDebtRequestRepository(t *testing.T) *mock.MockDebtRequestRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockDebtRequestRepository(ctrl)
	return repo
}

func generateSchemeRepository(t *testing.T) *mock.MockSchemeRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockSchemeRepository(ctrl)
	return repo
}

func generateSessionRegiserRepository(t *testing.T) *mock.MockSessionRegisterRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	repo := mock.NewMockSessionRegisterRepository(ctrl)
	return repo
}

func TestLinkTokenUseCase_RequestLinkToken(t *testing.T) {
	type fields struct {
		organizationOIN           string
		linkTokenRepository       applinkprocess.LinkTokenRepository
		debtRequestRepository     applinkprocess.DebtRequestRepository
		schemeRepository          applinkprocess.SchemeRepository
		sessionRegisterRepository applinkprocess.SessionRegisterRepository
	}
	type args struct {
		oin           string
		debtRequestID string
		sessionToken  string
	}
	tests := []struct {
		name              string
		fields            fields
		args              args
		expectedLinkToken applinkprocess.LinkToken
		expectedError     error
	}{
		{
			name: "OIN is empty",
			fields: fields{
				organizationOIN:           DummyOIN,
				linkTokenRepository:       generateLinkTokenRepository(t),
				debtRequestRepository:     generateDebtRequestRepository(t),
				schemeRepository:          generateSchemeRepository(t),
				sessionRegisterRepository: generateSessionRegiserRepository(t),
			},
			args: args{
				oin:           "",
				debtRequestID: "my-request-id",
				sessionToken:  "",
			},
			expectedLinkToken: "",
			expectedError:     applinkprocess.ErrOINEmpty,
		},
		{
			name: "debt request ID is empty",
			fields: fields{
				organizationOIN:           DummyOIN,
				linkTokenRepository:       generateLinkTokenRepository(t),
				debtRequestRepository:     generateDebtRequestRepository(t),
				schemeRepository:          generateSchemeRepository(t),
				sessionRegisterRepository: generateSessionRegiserRepository(t),
			},
			args: args{
				oin:           DummyOIN,
				debtRequestID: "",
				sessionToken:  "",
			},
			expectedLinkToken: "",
			expectedError:     applinkprocess.ErrDebtRequestIdEmpty,
		},
		{
			name: "session token is empty",
			fields: fields{
				organizationOIN:           DummyOIN,
				linkTokenRepository:       generateLinkTokenRepository(t),
				debtRequestRepository:     generateDebtRequestRepository(t),
				schemeRepository:          generateSchemeRepository(t),
				sessionRegisterRepository: generateSessionRegiserRepository(t),
			},
			args: args{
				oin:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "",
			},
			expectedLinkToken: "",
			expectedError:     applinkprocess.ErrSessionTokenEmpty,
		},
		{
			name: "session token is invalid",
			fields: fields{
				organizationOIN:       DummyOIN,
				linkTokenRepository:   generateLinkTokenRepository(t),
				debtRequestRepository: generateDebtRequestRepository(t),
				schemeRepository:      generateSchemeRepository(t),
				sessionRegisterRepository: func() applinkprocess.SessionRegisterRepository {
					repo := generateSessionRegiserRepository(t)
					repo.EXPECT().GetSession("invalid").Return(nil, nil).AnyTimes()
					return repo
				}(),
			},
			args: args{
				oin:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "invalid",
			},
			expectedLinkToken: "",
			expectedError:     applinkprocess.ErrSessionTokenInvalid,
		},
		{
			name: "failed to fetch session token",
			fields: fields{
				organizationOIN:       DummyOIN,
				linkTokenRepository:   generateLinkTokenRepository(t),
				debtRequestRepository: generateDebtRequestRepository(t),
				schemeRepository:      generateSchemeRepository(t),
				sessionRegisterRepository: func() applinkprocess.SessionRegisterRepository {
					repo := generateSessionRegiserRepository(t)
					repo.EXPECT().GetSession("mock-session-token").Return(nil, errors.New("arbitrary error")).AnyTimes()
					return repo
				}(),
			},
			args: args{
				oin:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "mock-session-token",
			},
			expectedLinkToken: "",
			expectedError:     errors.New("unable to check if session token exists: arbitrary error"),
		},
		{
			name: "fails to fetch linkToken",
			fields: fields{
				organizationOIN: DummyOIN,
				linkTokenRepository: func() applinkprocess.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().GetLinkToken(gomock.Any()).Return(nil, errors.New("arbitrary error")).AnyTimes()
					return repo
				}(),
				debtRequestRepository: generateDebtRequestRepository(t),
				schemeRepository:      generateSchemeRepository(t),
				sessionRegisterRepository: func() applinkprocess.SessionRegisterRepository {
					repo := generateSessionRegiserRepository(t)
					sessionTokenString := "mock-session-token"
					sessionToken := (*applinkprocess.SessionToken)(&sessionTokenString)
					repo.EXPECT().GetSession(sessionTokenString).Return(sessionToken, nil).AnyTimes()
					return repo
				}(),
			},
			args: args{
				oin:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "mock-session-token",
			},
			expectedLinkToken: "",
			expectedError:     errors.New("unable to get link token: arbitrary error"),
		},
		{
			name: "fails to fetch organization",
			fields: fields{
				organizationOIN: DummyOIN,
				linkTokenRepository: func() applinkprocess.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().GetLinkToken(gomock.Any()).Return(nil, nil).AnyTimes()
					return repo
				}(),
				debtRequestRepository: generateDebtRequestRepository(t),
				schemeRepository: func() applinkprocess.SchemeRepository {
					repo := generateSchemeRepository(t)
					repo.EXPECT().GetOrganizationByOIN(DummyOIN).Return(nil, errors.New("arbitrary error")).AnyTimes()
					return repo
				}(),
				sessionRegisterRepository: func() applinkprocess.SessionRegisterRepository {
					repo := generateSessionRegiserRepository(t)
					sessionTokenString := "mock-session-token"
					sessionToken := (*applinkprocess.SessionToken)(&sessionTokenString)
					repo.EXPECT().GetSession(sessionTokenString).Return(sessionToken, nil).AnyTimes()
					return repo
				}(),
			},
			args: args{
				oin:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "mock-session-token",
			},
			expectedLinkToken: "",
			expectedError:     errors.New("failed to fetch organization for oin: arbitrary error"),
		},
		{
			name: "oin is not present in the debt request",
			fields: fields{
				organizationOIN: DummyOIN,
				linkTokenRepository: func() applinkprocess.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().GetLinkToken(gomock.Any()).Return(nil, nil).AnyTimes()
					return repo
				}(),
				debtRequestRepository: func() applinkprocess.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(&DummyOrganization, "my-request-id").Return(&applinkprocess.SchuldenRequest{
						Organizations: []*applinkprocess.Organization{
							{
								OIN: "",
							},
						},
					}, nil).AnyTimes()
					return repo
				}(),
				schemeRepository: func() applinkprocess.SchemeRepository {
					repo := generateSchemeRepository(t)
					repo.EXPECT().GetOrganizationByOIN(DummyOIN).Return(&DummyOrganization, nil).AnyTimes()
					return repo
				}(),
				sessionRegisterRepository: func() applinkprocess.SessionRegisterRepository {
					repo := generateSessionRegiserRepository(t)
					sessionTokenString := "mock-session-token"
					sessionToken := (*applinkprocess.SessionToken)(&sessionTokenString)
					repo.EXPECT().GetSession(sessionTokenString).Return(sessionToken, nil).AnyTimes()
					return repo
				}(),
			},
			args: args{
				oin:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "mock-session-token",
			},
			expectedLinkToken: "",
			expectedError:     errors.New("the oin of the source organization is not present in the schulden request"),
		},
		{
			name: "linkToken is already generated",
			fields: fields{
				organizationOIN: DummyOIN,
				linkTokenRepository: func() applinkprocess.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().GetLinkToken("my-request-id").Return(&DummyLinkToken, nil).AnyTimes()
					return repo
				}(),
				debtRequestRepository: generateDebtRequestRepository(t),
				schemeRepository:      generateSchemeRepository(t),
				sessionRegisterRepository: func() applinkprocess.SessionRegisterRepository {
					repo := generateSessionRegiserRepository(t)
					sessionTokenString := "mock-session-token"
					sessionToken := (*applinkprocess.SessionToken)(&sessionTokenString)
					repo.EXPECT().GetSession(sessionTokenString).Return(sessionToken, nil).AnyTimes()
					return repo
				}(),
			},
			args: args{
				oin:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "mock-session-token",
			},
			expectedLinkToken: DummyLinkToken,
			expectedError:     nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			linkTokenUseCase := applinkprocess.NewLinkTokenUseCase(tt.fields.organizationOIN, tt.fields.linkTokenRepository, tt.fields.debtRequestRepository, tt.fields.schemeRepository, tt.fields.sessionRegisterRepository)
			linkToken, err := linkTokenUseCase.RequestLinkToken(tt.args.oin, tt.args.debtRequestID, tt.args.sessionToken)
			assert.Equal(t, tt.expectedLinkToken, linkToken)
			assert.Equal(t, tt.expectedError, err)
		})
	}
}
