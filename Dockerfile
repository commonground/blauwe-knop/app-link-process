# Use go 1.x based on alpine image.
FROM golang:1.17.3-alpine AS build

ADD . /go/src/app-link-process/
ENV GO111MODULE on
WORKDIR /go/src/app-link-process
RUN go mod download
RUN go build -o dist/bin/app-link-process ./cmd/app-link-process

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/app-link-process/dist/bin/app-link-process /usr/local/bin/app-link-process

# Add non-priveleged user
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/app-link-process"]
