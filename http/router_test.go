// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"testing"

	"github.com/golang/mock/gomock"

	"applinkprocess"
	"applinkprocess/mock"
)

var DummyLinkToken = applinkprocess.LinkToken("dummy-link-token")

func generateLinkTokenRepository(t *testing.T) *mock.MockLinkTokenRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockLinkTokenRepository(ctrl)
	return repo
}

func generateDebtRequestRepository(t *testing.T) *mock.MockDebtRequestRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockDebtRequestRepository(ctrl)
	return repo
}

func generateSchemeRepository(t *testing.T) *mock.MockSchemeRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockSchemeRepository(ctrl)
	return repo
}

func generateSessionRegisterRepository(t *testing.T) *mock.MockSessionRegisterRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockSessionRegisterRepository(ctrl)
	return repo
}
