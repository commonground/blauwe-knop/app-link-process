// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"log"
	"net/http"

	"github.com/go-chi/render"

	"applinkprocess"
)

func handlerRequestLinkToken(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	linkTokenUseCase, _ := ctx.Value(linkTokenUseCaseKey).(*applinkprocess.LinkTokenUseCase)

	debtRequestID := req.URL.Query().Get("debtRequestId")
	oin := req.URL.Query().Get("oin")
	sessionToken := req.URL.Query().Get("sessionToken")
	linkToken, err := linkTokenUseCase.RequestLinkToken(oin, debtRequestID, sessionToken)

	if err == applinkprocess.ErrDebtRequestIdEmpty {
		log.Printf("a debt request ID is required")
		http.Error(w, "debt request ID required", http.StatusBadRequest)
		return
	} else if err == applinkprocess.ErrOINEmpty {
		log.Printf("a OIN is required")
		http.Error(w, "OIN is required", http.StatusBadRequest)
		return
	} else if err == applinkprocess.ErrSessionTokenEmpty {
		log.Printf("a sessionToken is required")
		http.Error(w, "sessionToken is required", http.StatusBadRequest)
		return
	} else if err == applinkprocess.ErrSessionTokenInvalid {
		log.Printf("session invalid")
		http.Error(w, "session invalid", http.StatusUnauthorized)
		return
	} else if err != nil {
		log.Printf("error requesting link token: %v", err)
		http.Error(w, "error processing request", http.StatusInternalServerError)
		return
	}

	render.PlainText(w, req, string(linkToken))
}
