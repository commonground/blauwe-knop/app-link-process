// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"applinkprocess"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type SessionRegisterRepositoryEndpoint struct {
	baseURL string
	apiKey  string
}

type sessionTokenModel struct {
	SessionToken string `json:"sessionToken"`
}

func NewSessionRegisterRepository(baseURL string, apiKey string) *SessionRegisterRepositoryEndpoint {
	return &SessionRegisterRepositoryEndpoint{
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

func (s *SessionRegisterRepositoryEndpoint) GetSession(sessionToken string) (*applinkprocess.SessionToken, error) {
	requestBody := sessionTokenModel{sessionToken}
	requestBodyAsJson, err := json.Marshal(requestBody)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	url := fmt.Sprintf("%s/sessions/", s.baseURL)
	req, err := http.NewRequest(http.MethodGet, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to get session request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to get session: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving session: %d", resp.StatusCode)
	}

	sessionResponse := &sessionTokenModel{}
	err = json.NewDecoder(resp.Body).Decode(sessionResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	log.Printf("SessionToken: %s", sessionResponse.SessionToken)

	return (*applinkprocess.SessionToken)(&sessionResponse.SessionToken), nil
}

func (s *SessionRegisterRepositoryEndpoint) GetHealthCheck() healthcheck.Result {
	name := "session-register"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	body, err := ioutil.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}
	log.Printf("resp.Body: %s", string(body))

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
