// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"context"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"

	"applinkprocess"
)

func NewRouter(linkTokenUseCase *applinkprocess.LinkTokenUseCase) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Route("/auth/request-link-token", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), linkTokenUseCaseKey, linkTokenUseCase)
			handlerRequestLinkToken(w, r.WithContext(ctx))
		})

		healthCheckHandler := healthcheck.NewHandler("app-link-process", linkTokenUseCase.GetHealthChecks())
		r.Route("/health", func(r chi.Router) {
			r.Get("/", healthCheckHandler.HandleHealth)
			r.Get("/check", healthCheckHandler.HandlerHealthCheck)
		})

	})

	return r
}

type key int

const (
	linkTokenUseCaseKey key = iota
)
