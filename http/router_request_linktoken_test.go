// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"applinkprocess"
	http_infra "applinkprocess/http"
)

const DummyOIN = "0000000000"

var DummyOrganization = applinkprocess.Organization{
	OIN:            DummyOIN,
	RegistratorURL: "http://registrator-url",
}

func Test_CreateRouter_RequestLinkToken(t *testing.T) {
	type fields struct {
		organizationOIN           string
		linkTokenRepository       applinkprocess.LinkTokenRepository
		debtRequestRepository     applinkprocess.DebtRequestRepository
		schemeRepository          applinkprocess.SchemeRepository
		sessionRegisterRepository applinkprocess.SessionRegisterRepository
	}
	type args struct {
		OIN           string
		debtRequestID string
		sessionToken  string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"without specifying an OIN",
			fields{
				organizationOIN:           DummyOIN,
				linkTokenRepository:       generateLinkTokenRepository(t),
				debtRequestRepository:     generateDebtRequestRepository(t),
				schemeRepository:          generateSchemeRepository(t),
				sessionRegisterRepository: generateSessionRegisterRepository(t),
			},
			args{
				OIN:           "",
				debtRequestID: "my-request-id",
				sessionToken:  "",
			},
			http.StatusBadRequest,
			"OIN is required\n",
		},
		{
			"without specifying a request ID",
			fields{
				organizationOIN:           DummyOIN,
				linkTokenRepository:       generateLinkTokenRepository(t),
				debtRequestRepository:     generateDebtRequestRepository(t),
				schemeRepository:          generateSchemeRepository(t),
				sessionRegisterRepository: generateSessionRegisterRepository(t),
			},
			args{
				OIN:           DummyOIN,
				debtRequestID: "",
				sessionToken:  "",
			},
			http.StatusBadRequest,
			"debt request ID required\n",
		},
		{
			"without specifying a session Token",
			fields{
				organizationOIN:           DummyOIN,
				linkTokenRepository:       generateLinkTokenRepository(t),
				debtRequestRepository:     generateDebtRequestRepository(t),
				schemeRepository:          generateSchemeRepository(t),
				sessionRegisterRepository: generateSessionRegisterRepository(t),
			},
			args{
				OIN:           DummyOIN,
				debtRequestID: "debt-request-id",
				sessionToken:  "",
			},
			http.StatusBadRequest,
			"sessionToken is required\n",
		},
		{
			"session token invalid",
			fields{
				organizationOIN:       DummyOIN,
				linkTokenRepository:   generateLinkTokenRepository(t),
				debtRequestRepository: generateDebtRequestRepository(t),
				schemeRepository:      generateSchemeRepository(t),
				sessionRegisterRepository: func() applinkprocess.SessionRegisterRepository {
					repo := generateSessionRegisterRepository(t)
					repo.EXPECT().GetSession("invalid").Return(nil, nil).AnyTimes()
					return repo
				}(),
			},
			args{
				OIN:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "invalid",
			},
			http.StatusUnauthorized,
			"session invalid\n",
		},
		{
			"failed to validate session token",
			fields{
				organizationOIN: DummyOIN,
				linkTokenRepository: func() applinkprocess.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().GetLinkToken(gomock.Any()).Return(nil, nil).AnyTimes()
					return repo
				}(),
				debtRequestRepository: generateDebtRequestRepository(t),
				schemeRepository: func() applinkprocess.SchemeRepository {
					repo := generateSchemeRepository(t)
					repo.EXPECT().GetOrganizationByOIN(DummyOIN).Return(&DummyOrganization, nil).AnyTimes()
					return repo
				}(),
				sessionRegisterRepository: func() applinkprocess.SessionRegisterRepository {
					repo := generateSessionRegisterRepository(t)
					sessionTokenString := "mock-session-token"
					sessionToken := (*applinkprocess.SessionToken)(&sessionTokenString)
					repo.EXPECT().GetSession(sessionTokenString).Return(sessionToken, errors.New("arbitrary error")).AnyTimes()
					return repo
				}(),
			},
			args{
				OIN:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "mock-session-token",
			},
			http.StatusInternalServerError,
			"error processing request\n",
		},
		{
			"failed to get link token",
			fields{
				organizationOIN: DummyOIN,
				linkTokenRepository: func() applinkprocess.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().GetLinkToken(gomock.Any()).Return(nil, errors.New("arbitrary error")).AnyTimes()
					return repo
				}(),
				debtRequestRepository: generateDebtRequestRepository(t),
				schemeRepository:      generateSchemeRepository(t),
				sessionRegisterRepository: func() applinkprocess.SessionRegisterRepository {
					repo := generateSessionRegisterRepository(t)
					sessionTokenString := "mock-session-token"
					sessionToken := (*applinkprocess.SessionToken)(&sessionTokenString)
					repo.EXPECT().GetSession(sessionTokenString).Return(sessionToken, nil).AnyTimes()
					return repo
				}(),
			},
			args{
				OIN:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "mock-session-token",
			},
			http.StatusInternalServerError,
			"error processing request\n",
		},
		{
			"failed to fetch debt request",
			fields{
				organizationOIN: DummyOIN,
				linkTokenRepository: func() applinkprocess.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().GetLinkToken(gomock.Any()).Return(nil, nil).AnyTimes()
					return repo
				}(),
				debtRequestRepository: func() applinkprocess.DebtRequestRepository {
					repo := generateDebtRequestRepository(t)
					repo.EXPECT().Get(&DummyOrganization, "my-request-id").Return(nil, errors.New("arbitrary error")).AnyTimes()
					return repo
				}(),
				schemeRepository: func() applinkprocess.SchemeRepository {
					repo := generateSchemeRepository(t)
					repo.EXPECT().GetOrganizationByOIN(DummyOIN).Return(&DummyOrganization, nil).AnyTimes()
					return repo
				}(),
				sessionRegisterRepository: func() applinkprocess.SessionRegisterRepository {
					repo := generateSessionRegisterRepository(t)
					sessionTokenString := "mock-session-token"
					sessionToken := (*applinkprocess.SessionToken)(&sessionTokenString)
					repo.EXPECT().GetSession(sessionTokenString).Return(sessionToken, nil).AnyTimes()
					return repo
				}(),
			},
			args{
				OIN:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "mock-session-token",
			},
			http.StatusInternalServerError,
			"error processing request\n",
		},
		{
			"link token is already present for request id",
			fields{
				organizationOIN: DummyOIN,
				linkTokenRepository: func() applinkprocess.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().GetLinkToken("my-request-id").Return(&DummyLinkToken, nil).AnyTimes()
					return repo
				}(),
				debtRequestRepository: generateDebtRequestRepository(t),
				sessionRegisterRepository: func() applinkprocess.SessionRegisterRepository {
					repo := generateSessionRegisterRepository(t)
					sessionTokenString := "mock-session-token"
					sessionToken := (*applinkprocess.SessionToken)(&sessionTokenString)
					repo.EXPECT().GetSession(sessionTokenString).Return(sessionToken, nil).AnyTimes()
					return repo
				}(),
			},
			args{
				OIN:           DummyOIN,
				debtRequestID: "my-request-id",
				sessionToken:  "mock-session-token",
			},
			http.StatusOK,
			"dummy-link-token",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()
			linkTokenUseCase := applinkprocess.NewLinkTokenUseCase(test.fields.organizationOIN, test.fields.linkTokenRepository, test.fields.debtRequestRepository, test.fields.schemeRepository, test.fields.sessionRegisterRepository)
			router := http_infra.NewRouter(linkTokenUseCase)
			w := httptest.NewRecorder()

			request := httptest.NewRequest("GET", fmt.Sprintf("/auth/request-link-token?debtRequestId=%s&oin=%s&sessionToken=%s", test.args.debtRequestID, test.args.OIN, test.args.sessionToken), nil)

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
