// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"applinkprocess"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type schemeResponseModel []*struct {
	OIN                                 string `json:"oin"`
	Name                                string `json:"name"`
	APIBaseURL                          string `json:"apiBaseUrl"`
	LoginURL                            string `json:"loginUrl"`
	AppLinkProcessURL                   string `json:"appLinkProcessUrl"`
	IsRegistrator                       bool   `json:"isRegistrator"`
	RegistratorURL                      string `json:"registratorUrl"`
	DebtRequestRegisterURL              string `json:"debtRequestRegisterUrl"`
	DebtRequestRegisterOrganizationName string `json:"debtRequestRegisterOrganizationName"`
	DebtRequestRegisterServiceName      string `json:"debtRequestRegisterServiceName"`
}

type SchemeRepository struct {
	baseURL string
}

func NewSchemeRepository(baseURL string) *SchemeRepository {
	return &SchemeRepository{
		baseURL: baseURL,
	}
}

func (s *SchemeRepository) GetOrganizationByOIN(oin string) (*applinkprocess.Organization, error) {
	resp, err := http.Get(s.baseURL)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch scheme: %v", err)
	}

	schemeResponse := &schemeResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(schemeResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	organizations := mapSchemeResponseToModels(schemeResponse)

	for _, organization := range organizations {
		if organization.OIN == oin {
			return organization, nil
		}
	}

	return nil, nil
}

func mapSchemeResponseToModels(schemeResponse *schemeResponseModel) []*applinkprocess.Organization {
	var result []*applinkprocess.Organization

	for _, organization := range *schemeResponse {
		result = append(result, &applinkprocess.Organization{
			OIN:                                 organization.OIN,
			Name:                                organization.Name,
			APIBaseURL:                          organization.APIBaseURL,
			LoginURL:                            organization.LoginURL,
			RegistratorURL:                      organization.RegistratorURL,
			DebtRequestRegisterURL:              organization.DebtRequestRegisterURL,
			DebtRequestRegisterOrganizationName: organization.DebtRequestRegisterOrganizationName,
			DebtRequestRegisterServiceName:      organization.DebtRequestRegisterServiceName,
		})
	}

	return result
}

func (s *SchemeRepository) GetHealthCheck() healthcheck.Result {
	name := "scheme"
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, s.baseURL, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	if response.StatusCode != http.StatusOK {
		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	return healthcheck.Result{
		Name:         name,
		Status:       healthcheck.StatusOK,
		ResponseTime: time.Since(start).Seconds(),
		HealthChecks: nil,
	}
}
