// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"applinkprocess"

	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
)

type linkTokenResponseModel struct {
	LinkToken string `json:"linkToken"`
}

type LinkRegisterRepository struct {
	baseURL string
	apiKey  string
}

func NewLinkRegisterRepository(baseURL string, apiKey string) *LinkRegisterRepository {
	return &LinkRegisterRepository{
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

func (s *LinkRegisterRepository) GetLinkToken(debtRequestID string) (*applinkprocess.LinkToken, error) {
	url := fmt.Sprintf("%s/link-tokens?debtRequestId=%s", s.baseURL, debtRequestID)
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create get link token request: %v", err)
	}

	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch link token: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, nil
	} else if resp.StatusCode != http.StatusOK {
		log.Printf("dri: %s", debtRequestID)
		return nil, fmt.Errorf("unexpected status code while fetching link token: %d", resp.StatusCode)
	}

	linkTokenResponse := &linkTokenResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(linkTokenResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return mapResponseToModel(linkTokenResponse), nil
}

func (s *LinkRegisterRepository) CreateLinkToken(debtRequestID string) (*applinkprocess.LinkToken, error) {
	requestBody := map[string]string{"debtRequestId": debtRequestID}
	requestBodyAsJson, err := json.Marshal(requestBody)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	url := fmt.Sprintf("%s/link-tokens", s.baseURL)
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create get link token request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to create link token: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while creating link token: %d", resp.StatusCode)
	}

	linkTokenResponse := &linkTokenResponseModel{}
	err = json.NewDecoder(resp.Body).Decode(linkTokenResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return mapResponseToModel(linkTokenResponse), nil
}

type addBSNToLinkTokenRequestBody struct {
	BSN string `json:"bsn"`
}

func (s *LinkRegisterRepository) AddBSNToLinkToken(bsn string, linkToken *applinkprocess.LinkToken) error {
	requestBody := addBSNToLinkTokenRequestBody{
		BSN: bsn,
	}
	requestBodyAsJson, err := json.Marshal(requestBody)
	if err != nil {
		return fmt.Errorf("failed to marshal request body: %v", err)
	}
	url := fmt.Sprintf("%s/link-tokens/%v", s.baseURL, string(*linkToken))
	req, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return fmt.Errorf("failed to create request: %v", err)
	}

	req.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("failed to perform request: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return fmt.Errorf("failed to read response body: %v", err)
		}

		return fmt.Errorf("unexpected status while assigning bsn to link token: %d. message: %s", resp.StatusCode, body)
	}

	return nil
}

func mapResponseToModel(linkTokenResponse *linkTokenResponseModel) *applinkprocess.LinkToken {
	result := applinkprocess.LinkToken(linkTokenResponse.LinkToken)
	return &result
}

func (s *LinkRegisterRepository) GetHealthCheck() healthcheck.Result {
	name := "link-register"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	start := time.Now()
	timeout := 10 * time.Second

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}

	body, err := ioutil.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: nil,
		}
	}
	log.Printf("resp.Body: %s", string(body))

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
