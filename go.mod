module applinkprocess

go 1.17

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/golang/mock v1.6.0
	github.com/stretchr/testify v1.7.0
	github.com/svent/go-flags v0.0.0-20141123140740-4bcbad344f03
	gitlab.com/commonground/blauwe-knop/health-checker v0.0.2
	go.uber.org/zap v1.19.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
