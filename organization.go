// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package applinkprocess

type Organization struct {
	OIN                                 string
	Name                                string
	APIBaseURL                          string
	LoginURL                            string
	RegistratorURL                      string
	DebtRequestRegisterURL              string
	DebtRequestRegisterOrganizationName string
	DebtRequestRegisterServiceName      string
}
