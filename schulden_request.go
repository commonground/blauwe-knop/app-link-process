// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package applinkprocess

type SchuldenRequest struct {
	Id            string
	BSN           string
	Organizations []*Organization
}
