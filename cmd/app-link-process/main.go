// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"

	"applinkprocess"
	file_infra "applinkprocess/file"
	http_infra "applinkprocess/http"
)

type options struct {
	Organization           string `long:"organization" env:"ORGANIZATION" description:"Organization name"`
	OrganizationOIN        string `long:"organization-oin" env:"ORGANIZATION_OIN" description:"Organization OIN"`
	ListenAddress          string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:8089" description:"Address for the app-link-process api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	SchemeURI              string `long:"scheme-uri" env:"SCHEME_URI" default:"https://gitlab.com/commonground/blauwe-knop/scheme/-/raw/master/organizations.json" description:"URI of the scheme containing the organizations. Can be a file path or a URL"`
	OutwayAddress          string `long:"outway-address" env:"OUTWAY_ADDRESS" default:"" description:"Outway address for NLX. Leave empty to omit NLX."`
	LinkRegisterAddress    string `long:"link-register-address" env:"LINK_REGISTER_ADDRESS" default:"http://localhost:8085" description:"Link register address."`
	LinkRegisterAPIKey     string `long:"link-register-api-key" env:"LINK_REGISTER_API_KEY" default:"" description:"API key to use when calling the link-register service."`
	SessionRegisterAddress string `long:"session-register-address" env:"SESSION_REGISTER_ADDRESS" default:"http://localhost:8084" description:"Session register address."`
	SessionRegisterAPIKey  string `long:"session-register-api-key" env:"SESSION_REGISTER_API_KEY" default:"" description:"API key to use when calling the session-register service."`

	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	if cliOptions.Organization == "" {
		log.Fatalf("please specify an organization")
	}

	if cliOptions.OrganizationOIN == "" {
		log.Fatalf("please specify an organization OIN")
	}

	if cliOptions.SchemeURI == "" {
		log.Fatalf("please specify the scheme URI")
	}

	sessionRegisterRepository := http_infra.NewSessionRegisterRepository(cliOptions.SessionRegisterAddress, cliOptions.SessionRegisterAPIKey)

	debtRequestProcessRepository := http_infra.NewDebtRequestRegisterRepository(cliOptions.OutwayAddress)

	linkRegisterRepository := http_infra.NewLinkRegisterRepository(cliOptions.LinkRegisterAddress, cliOptions.LinkRegisterAPIKey)

	schemeURL, err := url.Parse(cliOptions.SchemeURI)
	if err != nil {
		log.Fatalf("error parsing scheme URI %v", err)
	}

	var schemeRepository applinkprocess.SchemeRepository
	if schemeURL.Scheme == "file" {
		schemeRepository, err = file_infra.NewSchemeRepository(schemeURL.Hostname())
		if err != nil {
			log.Fatalf("error create file scheme repository: %v", err)
		}
	} else {
		schemeRepository = http_infra.NewSchemeRepository(cliOptions.SchemeURI)
	}

	linkTokenUseCase := applinkprocess.NewLinkTokenUseCase(cliOptions.OrganizationOIN, linkRegisterRepository, debtRequestProcessRepository, schemeRepository, sessionRegisterRepository)
	router := http_infra.NewRouter(linkTokenUseCase)

	logger.Info(fmt.Sprintf("start listening on %s", cliOptions.ListenAddress))

	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		if err != http.ErrServerClosed {
			panic(err)
		}
	}
}
