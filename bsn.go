// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package applinkprocess

import (
	"errors"
	"regexp"
)

type BSN struct {
	value string
}

func NewBSN(value string) (*BSN, error) {
	if len(value) != 9 {
		return nil, errors.New("bsn should consist of 9 numbers")
	}

	re := regexp.MustCompile(`\d{9}`)
	if !re.MatchString(value) {
		return nil, errors.New("must be numeric")
	}

	return &BSN{
		value: value,
	}, nil
}

func (d BSN) String() string {
	return d.value
}
